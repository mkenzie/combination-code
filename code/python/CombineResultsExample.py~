"""
The following is an example of a combination of three measurements
in two decays channels, Bs0->J/psi KK and Bs0->J/psi pi pi,
with an external constraint on the B0 decay width. The method is
similar to the one described in Section 11 here: 
https://arxiv.org/pdf/1906.08356.pdf
While the inputs are based on real measurements of the LHCb experiment, 
this is a simplified example for purely illustrative purposes and is 
not meant to be interpreted as an LHCb average of these decay modes.
"""

import sys
import numpy as np
import json
import pylab as pl
import matplotlib.pyplot as plt
from iminuit import Minuit
from HelperFunctions import showIminuitResult, showIminuitMinosResult, showIminuitResultCorrmat, outputMinuitResult, EvaluateForMinuit
from HelperFunctions import printLatexTable, printChi2nDoF, makeResidualPlot
from ResultSet import ResultList, readResultListJSON

import Translators


#-------------- Main code to perform the combination -------------
def doAverage( reslist, pars ):
    """
    Main code to perform the combination
    reslist: the ResultList containg all ResultSets to be averaged
    pars: the names of the target parameters for the average
    """
    np.set_printoptions(5)
   
    #Create the EvaluateFotMinuit object which provides the FCN interface
    eval = EvaluateForMinuit( reslist, pars )

    # Get the dictionary with all parameters, their start values and their initial setepsize (errors)
    svals = reslist.getStartValues( pars )

    #Fix or update any parameters you want to - examples below
    #svals.update({'fix_dms':True})
    #svals.update({'dms':17.757})
    #svals.update({'fix_lamb':True})
    #svals.update({'lamb':1.})

    # Now get the numerical list of starting values out of the dictionary in the correct order for the Minuit constructor
    start =[ svals[p] for p in pars ]
    
    # Create Minimiser with special constructor.
    # The names and order of parameters is given by pars
    m = Minuit.from_array_func(eval.fcn, start, name=pars, errordef=1.0, print_level=0, **svals )
    m.tol= m.tol*0.0001
    
    #Do fit
    m.migrad()
    m.minos()
    
    return m


#-------------- Main -------------
def main():

    print('\nTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT')
    print('    ResultSet Combination Tool ')
    print('LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL')
    
    #Set up your i/o paths
    inputpath = '../inputs/'
    outputpath = 'outputs/'
    
    #This is where you configure your list of input JSON files to be included in the average (shold be in the inputpath)
    '''
    inputfilelist = [
        'ResultList-Example-JpsiKK.json',
        'ResultList-Example-JpsiPiPi.json',
        'ResultList-Gammad.json'
    ]
    '''
    inputfilelist = [
        'ResultList-LHCb-JpsiKK.json',
        'ResultList-LHCb-JpsiPiPi.json',
        'ResultList-Gammad.json'
    ]
    print('\nThe combination has been configured with these Input file(s): ',inputfilelist)
    
    #This is where you configure your list of input JSON files to configure any intercorrelations  (optional)
    intercorrelationFileList = [
        'IntercorrelationMaps-All.json'
    ]
    print('\nThe combination has been configured with these intercorrelations file(s): ',intercorrelationFileList)

    #Now choose what you want to include in this average
    doStatisticalCorrelations = True
    doSystematics = True
    doSystematicCorrelations = True
    doIntercorrelations = True
    
    # This is where you set target average parameters to fit to, in the order you want Minuit to list them
    # Everything else will be driven off of this list automatically
    pars = ['phis', 'lamb','lambJpsiPiPi','gamma', 'deltaGammas', 'dms', 'AperpSq', 'AzeroSq', 'perp', 'para', \
            'FS1', 'FS2', 'FS3', 'FS4', 'FS5', 'FS6', 'deltaS1', 'deltaS2', 'deltaS3', 'deltaS4', 'deltaS5', 'deltaS6', \
            'gammaD' ]


    #This is the main function to read in all the JSON files and get them into the ResulList which gets returned.
    reslist = readResultListJSON(inputfilelist, icfilelist=intercorrelationFileList, inputpath=inputpath, outputpath=outputpath , \
        doStatCorr=doStatisticalCorrelations, doSyst=doSystematics, doSystCorr=doSystematicCorrelations, doInterCorr=doIntercorrelations )
        
    # Add all translators
    # You must add translators to the Translators module if your target average parameters are not the same as the
    # parameters in the input ResultList.
    Translators.AddAllTranslators( reslist )
                                 
    #Show all systematic errors and full correlation matrix
    #reslist.showAll(PrintCorrMat=False,PrintCovMat=False)

    #Just show parameters and overall correlation matrix
    reslist.show()

    #Show the set of unique parameters for info
    ####print('\n It has unique parameters: ', ['{:10s}'.format(par) for par in reslist.getUniqueParameters()])
    
    print('\n-------------Doing the combination--------------')
    
    # This is the function to make the comination
    minfit = doAverage( reslist, pars )

    #Print the minuit result for the averages
    showIminuitResult( minfit )
    
    #Minos errors
    showIminuitMinosResult( minfit )
    
    #Show the simple average for comparison
    reslist.showSimpleAverage(paramorder = pars)
    
    #Show the final correlation matrix
    showIminuitResultCorrmat( minfit )
    
    #Print chi2/nDoF
    chisqndof = printChi2nDoF(reslist, minfit)

    #Make residuals plot all points
    makeResidualPlot( chisqndof, reslist, outputpath+'PullPlot.pdf' )

    #output the full result in JSON format for use in a subsequent average
    reslist.outputSimpleAverage(outputFileName=outputpath+'ExampleSimpleAverageResultSet.json', title='Simp.Avg.' )
    outputMinuitResult( minfit, outputFileName=outputpath+'ExampleCombinationOutputResultSet.json', title='ExampleComb', minos=True )
    
    #Output a latex table of the corrmat
    #The latexLAbels dictionary must contain an entry for at least every parameter appaaring in pars defined above.
    #It may contin more unused labels
    suffix = 'Example-average'
    latexLabels = {'phis': '$\phi_{s}$', 'lamb': '$|\lambda|$','gamma': ' $\Gamma_{s}$ ', 'deltaGammas': '$\Delta\Gamma_s$', 'dms': '$\dms$', 'AperpSq': '$|A_{\perp}|^2$', 'AzeroSq': '$|A_0|^2$', 'perp': '$\delta_\perp -\delta_{0}$', 'para': '$\delta_\parallel -\delta_{0}$', 'FS1': '$|A^{1}_{S}|^{2}$', 'FS2': '$|A^{2}_{S}|^{2}$', 'FS3': '$|A^{3}_{S}|^{2}$', 'FS4': '$|A^{4}_{S}|^{2}$', 'FS5': '$|A^{5}_{S}|^{2}$', 'FS6': '$|A^{6}_{S}|^{2}$', 'deltaS1': ' $\delta^{1}_{S}-\delta_{\perp}$', 'deltaS2': ' $\delta^{2}_{S}-\delta_{\perp}$', 'deltaS3': ' $\delta^{3}_{S}-\delta_{\perp}$', 'deltaS4': ' $\delta^{4}_{S}-\delta_{\perp}$', 'deltaS5': ' $\delta^{5}_{S}-\delta_{\perp}$', 'deltaS6': ' $\delta^{6}_{S}-\delta_{\perp}$', 'gammaD': ' $\Gamma_{d}$', 'lambJpsiPiPi': '$\\left|\lambda_{J/\psi\pi\pi}\\right|$' }
    
    printLatexTable(minfit, latexLabels, outputFileName=outputpath+'CombinationCorrmatTable'+suffix+".tex")


main( )
